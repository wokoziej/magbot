<?xml version="1.0"?>
<!-- Revolute-Revolute Manipulator -->
<robot name="magbot" xmlns:xacro="http://www.ros.org/wiki/xacro">

  
  <!-- Constants for robot dimensions -->
  <xacro:property name="PI" value="3.1415926535897931"/>
  <xacro:property name="mass" value="1" /> <!-- arbitrary value for mass -->
  <xacro:property name="width" value="0.1" /> <!-- Square dimensions (widthxwidth) of beams -->
  <xacro:property name="height0" value="0.1" /> <!-- Link 0 -->
  <xacro:property name="height1" value="1.8" /> <!-- Link 1 -->
  <xacro:property name="height21" value="0.66" /> <!-- Link 2.1 -->
  <xacro:property name="height22" value="0.55" /> <!-- Link 2.2 -->
  <xacro:property name="height31" value="0.44" /> <!-- Link 3.1 -->
  <xacro:property name="height32" value="0.33" /> <!-- Link 3.2 -->
  <xacro:property name="heightG" value="0.2" /> <!-- Link 4 -->

  <xacro:property name="camera_link" value="0.05" /> <!-- Size of square 'camera' box -->
  <xacro:property name="axel_offset" value="0.05" /> <!-- Space btw top of beam and the each joint -->
  <xacro:property name="gripper_f_width" value = "0.5" />
  <xacro:property name="gripper_f_lenght"  value = "0.01" />
  <xacro:property name="gripper_f_height"  value = ".3" />
  <xacro:property name="damping" value="0.7" />
  <xacro:property name="friction" value="1"/>
  
  <!-- Import all Gazebo-customization elements, including Gazebo colors -->
  <xacro:include filename="$(find magbot_description)/urdf/magbot.gazebo" />
  <!-- Import Rviz colors -->
  <xacro:include filename="$(find magbot_description)/urdf/materials.xacro" />

  <!-- Used for fixing robot to Gazebo 'base_link' -->
  <link name="world"/>

  <joint name="platform_joint" type="fixed">
    <parent link="world"/>
    <child link="link0"/>
    <origin xyz="0 0 0" rpy="0 0 0"/>
    <axis xyz="0 0 1"/>
    <dynamics damping="0.7"/>
  </joint>


  <!-- Platform Link -->
  <link name="link0">
    <collision>
      <origin xyz="0 0 ${height0/2}" rpy="0 0 0"/>
      <geometry>
	<box size="${width*2} ${width*2} ${height0}"/>
      </geometry>
    </collision>

    <visual>
      <origin xyz="0 0 ${height0/2}" rpy="0 0 0"/>
      <geometry>
	<box size="${width*2} ${width*2} ${height0}"/>
      </geometry>
      <material name="orange"/>
    </visual>

    <inertial>
      <origin xyz="0 0 ${height0/2}" rpy="0 0 0"/>
      <mass value="${mass}"/>
      <inertia
	  ixx="${mass / 12.0 * (width*width*4 + height0*height0)}" ixy="0.0" ixz="0.0"
	  iyy="${mass / 12.0 * (height0*height0 + width*width*4)}" iyz="0.0"
	  izz="${mass / 12.0 * (width*width*4 + width*width*4)}"/>
    </inertial>
  </link>

  <joint name="joint0" type="continuous">
    <parent link="link0"/>
    <child link="link1"/>
    <origin xyz="0 0 0" rpy="0 0 0"/>
    <axis xyz="0 0 1"/>
    <dynamics damping="${damping}" friction="${friction}"/>
  </joint>
  
  <!-- Base Link -->
  <link name="link1">
    <collision>
      <origin xyz="0 0 ${height1/2}" rpy="0 0 0"/>
      <geometry>
  	<box size="${width} ${width} ${height1}"/>
      </geometry>
    </collision>

    <visual>
      <origin xyz="0 0 ${height1/2}" rpy="0 0 0"/>
      <geometry>
  	<box size="${width} ${width} ${height1}"/>
      </geometry>
      <material name="orange"/>
    </visual>

    <inertial>
      <origin xyz="0 0 ${height1/2}" rpy="0 0 0"/>
      <mass value="${mass}"/>
      <inertia
  	  ixx="${mass / 12.0 * (width*width + height1*height1)}" ixy="0.0" ixz="0.0"
  	  iyy="${mass / 12.0 * (height1*height1 + width*width)}" iyz="0.0"
  	  izz="${mass / 12.0 * (width*width + width*width)}"/>
    </inertial>
  </link>

  <joint name="joint11" type="continuous">
    <parent link="link1"/>
    <child link="link21"/>
    <origin xyz="0 ${width} ${height1 - axel_offset}" rpy="0 0 0"/>
    <axis xyz="0 1 0"/>
    <dynamics damping="${damping}" friction="${friction}"/>
    <!-- <limit lower="-2" upper="2" effort="0.2" velocity="0.2"/>     -->
  </joint>

  <!-- Middle Link 1-->
  <link name="link21">
    <collision>
      <origin xyz="0 0 ${height21/2 - axel_offset}" rpy="0 0 0"/>
      <geometry>
  	<box size="${width} ${width} ${height21}"/>
      </geometry>
    </collision>

    <visual>
      <origin xyz="0 0 ${height21/2 - axel_offset}" rpy="0 0 0"/>
      <geometry>
  	<box size="${width} ${width} ${height21}"/>
      </geometry>
      <material name="black"/>
    </visual>

    <inertial>
      <origin xyz="0 0 ${height21/2 - axel_offset}" rpy="0 0 0"/>
      <mass value="${mass}"/>
      <inertia
  	  ixx="${mass / 12.0 * (width*width + height21*height21)}" ixy="0.0" ixz="0.0"
  	  iyy="${mass / 12.0 * (height21*height21 + width*width)}" iyz="0.0"
  	  izz="${mass / 12.0 * (width*width + width*width)}"/>
    </inertial>
  </link>

  <joint name="joint12" type="continuous">
    <parent link="link21"/>
    <child link="link22"/>
    <origin xyz="0 0 ${height21}" rpy="0 0 0"/>
    <axis xyz="0 0 1"/>
    <dynamics damping="${damping}" friction="${friction}"/>
  </joint>

  <!-- Middle Link 2-->
  <link name="link22">
    <collision>
      <origin xyz="0 0 ${height22/2 - axel_offset}" rpy="0 0 0"/>
      <geometry>
  	<box size="${width} ${width} ${height22}"/>
      </geometry>
    </collision>

    <visual>
      <origin xyz="0 0 ${height22/2 - axel_offset}" rpy="0 0 0"/>
      <geometry>
  	<box size="${width} ${width} ${height22}"/>
      </geometry>
      <material name="black"/>
    </visual>

    <inertial>
      <origin xyz="0 0 ${height22/2 - axel_offset}" rpy="0 0 0"/>
      <mass value="${mass}"/>
      <inertia
  	  ixx="${mass / 12.0 * (width*width + height22*height22)}" ixy="0.0" ixz="0.0"
  	  iyy="${mass / 12.0 * (height22*height22 + width*width)}" iyz="0.0"
  	  izz="${mass / 12.0 * (width*width + width*width)}"/>
    </inertial>
  </link>
  
  <joint name="joint21" type="continuous">
    <parent link="link22"/>
    <child link="link31"/>
    <origin xyz="0 ${width} ${height22 - axel_offset*2}" rpy="0 0 0"/>
    <axis xyz="0 1 0"/>
    <dynamics damping="${damping}" friction="${friction}"/>
    <!-- <limit lower="-2" upper="2" effort="0.2" velocity="0.2"/>         -->
  </joint>

  <!-- Top Link -->
  <link name="link31">
    <collision>
      <origin xyz="0 0 ${height31/2 - axel_offset}" rpy="0 0 0"/>
      <geometry>
  	<box size="${width} ${width} ${height31}"/>
      </geometry>
    </collision>

    <visual>
      <origin xyz="0 0 ${height31/2 - axel_offset}" rpy="0 0 0"/>
      <geometry>
  	<box size="${width} ${width} ${height31}"/>
      </geometry>
      <material name="orange"/>
    </visual>

    <inertial>
      <origin xyz="0 0 ${height31/2 - axel_offset}" rpy="0 0 0"/>
      <mass value="${mass}"/>
      <inertia
  	  ixx="${mass / 12.0 * (width*width + height31*height31)}" ixy="0.0" ixz="0.0"
  	  iyy="${mass / 12.0 * (height31*height31 + width*width)}" iyz="0.0"
  	  izz="${mass / 12.0 * (width*width + width*width)}"/>
    </inertial>
  </link>

  <joint name="joint22" type="continuous">
    <parent link="link31"/>
    <child link="link32"/>
    <origin xyz="0 0 ${height31}" rpy="0 0 0"/>
    <axis xyz="0 0 1"/>
    <dynamics damping="${damping}" friction="${friction}"/>
  </joint>

  <link name="link32">
    <collision>
      <origin xyz="0 0 ${height32/2 - axel_offset}" rpy="0 0 0"/>
      <geometry>
  	<box size="${width} ${width} ${height32}"/>
      </geometry>
    </collision>

    <visual>
      <origin xyz="0 0 ${height32/2 - axel_offset}" rpy="0 0 0"/>
      <geometry>
  	<box size="${width} ${width} ${height32}"/>
      </geometry>
      <material name="pink"/>
    </visual>

    <inertial>
      <origin xyz="0 0 ${height32/2 - axel_offset}" rpy="0 0 0"/>
      <mass value="${mass}"/>
      <inertia
  	  ixx="${mass / 12.0 * (width*width + height32*height32)}" ixy="0.0" ixz="0.0"
  	  iyy="${mass / 12.0 * (height32*height32 + width*width)}" iyz="0.0"
  	  izz="${mass / 12.0 * (width*width + width*width)}"/>
    </inertial>
  </link>

  <joint name="joint3" type="continuous">
    <parent link="link32"/>
    <child link="linkG"/>
    <origin xyz="0 ${width} ${height32 - axel_offset*2}" rpy="0 0 0"/>
    <axis xyz="0 1 0"/>
    <dynamics damping="${damping}" friction="${friction}"/>
  </joint>

  <!-- Link for sensors/gripper -->
  <link name="linkG">
    <collision>
      <origin xyz="0 0 ${heightG/2 - axel_offset}" rpy="0 0 0"/>
      <geometry>
  	<box size="${width} ${width} ${heightG}"/>
      </geometry>
    </collision>

    <visual>
      <origin xyz="0 0 ${heightG/2 - axel_offset}" rpy="0 0 0"/>
      <geometry>
  	<box size="${width} ${width} ${heightG}"/>
      </geometry>
      <material name="orange"/>
    </visual>

    <inertial>
      <origin xyz="0 0 ${heightG/2 - axel_offset}" rpy="0 0 0"/>
      <mass value="${mass}"/>
      <inertia
  	  ixx="${mass / 12.0 * (width*width + heightG*heightG)}" ixy="0.0" ixz="0.0"
  	  iyy="${mass / 12.0 * (heightG*heightG + width*width)}" iyz="0.0"
  	  izz="${mass / 12.0 * (width*width + width*width)}"/>
    </inertial>
  </link>  



  <!-- fingers -->
  <joint name="finger_joint1" type="prismatic">
    <parent link="linkG"/>
    <child link="gripper_finger_link1"/>
    <origin xyz="0.0 0 ${gripper_f_height + 0.1}" />
    <axis xyz="0 1 0" />
    <limit effort="100" lower="0" upper="0.5" velocity="1.0"/>
    <safety_controller k_position="2000"
    		       k_velocity="2000"
    		       soft_lower_limit="${ -5 }"
    		       soft_upper_limit="${ 100 }"/>
    <dynamics damping="0.1" friction="1"/>
  </joint>

  <joint name="finger_joint2" type="prismatic">
    <parent link="linkG"/>
    <child link="gripper_finger_link2"/>
    <origin xyz="0.0 0.0 ${gripper_f_height + 0.1}" />
    <axis xyz="0 1 0" />
    <limit effort="100" lower="-0.5" upper="0" velocity="1.0"/>
    <safety_controller k_position="2000"
		       k_velocity="2000"
		       soft_lower_limit="${-5 }"
		       soft_upper_limit="${ 100 }"/>
    <dynamics damping="0.1" friction="1"/>
  </joint>


  <link name="gripper_finger_link1">
    <collision>
      <origin xyz="0 0 0" rpy="0 0 0"/>
      <geometry>
  	<box size="${gripper_f_width} ${gripper_f_lenght} ${gripper_f_height}"/>
      </geometry>
    </collision>

    <visual>
      <origin xyz="0 0 0" rpy="0 0 0"/>
      <geometry>
  	<box size="${gripper_f_width} ${gripper_f_lenght} ${gripper_f_height}"/>
      </geometry>
      <material name="black"/>
    </visual>


    <inertial>
      <origin xyz="0 0 0" rpy="0 0 0"/>
      <mass value="${mass}"/>
      <inertia
  	  ixx="${mass / 12.0 * (gripper_f_width*gripper_f_width + gripper_f_height*gripper_f_height)}" ixy="0.0" ixz="0.0"
  	  iyy="${mass / 12.0 * (gripper_f_height*gripper_f_height + gripper_f_width*gripper_f_width)}" iyz="0.0"
  	  izz="${mass / 12.0 * (gripper_f_width*gripper_f_width + gripper_f_width*gripper_f_width)}"/>
    </inertial>

    <!--
    <inertial>
      <origin xyz="0 0 0" rpy="0 0 0"/>
      <mass value="${mass}"/>
      <inertia
  	  ixx="1" ixy="0.0" ixz="0.0"
  	  iyy="1" iyz="0.0"
  	  izz="1"/>
    </inertial>-->
  </link>  


  <link name="gripper_finger_link2">
    <collision>
      <origin xyz="0 0 0" rpy="0 0 0"/>
      <geometry>
  	<box size="${gripper_f_width} ${gripper_f_lenght} ${gripper_f_height}"/>
      </geometry>
    </collision>

    <visual>
      <origin xyz="0 0 0" rpy="0 0 0"/>
      <geometry>
  	<box size="${gripper_f_width} ${gripper_f_lenght} ${gripper_f_height}"/>
      </geometry>
      <material name="orange"/>
    </visual>

    <inertial>
      <origin xyz="0 0 0" rpy="0 0 0"/>
      <mass value="${mass}"/>
      <inertia
  	  ixx="1" ixy="0.0" ixz="0.0"
  	  iyy="1" iyz="0.0"
  	  izz="1"/>
    </inertial>
  </link>  

  












  
  <joint name="hokuyo_joint" type="fixed">
    <axis xyz="0 1 0" />
    <origin xyz="0 0 ${heightG - axel_offset/2}" rpy="0 0 0"/>
    <parent link="linkG"/>
    <child link="hokuyo_link"/>
  </joint>

  <!-- Hokuyo Laser -->
  <link name="hokuyo_link">
    <collision>
      <origin xyz="0 0 0" rpy="0 0 0"/>
      <geometry>
  	<box size="0.1 0.1 0.1"/>
      </geometry>
    </collision>

    <visual>
      <origin xyz="0 0 0" rpy="0 0 0"/>
      <geometry>
        <mesh filename="package://magbot_description/meshes/hokuyo.dae"/>
      </geometry>
    </visual>

    <inertial>
      <mass value="1e-5" />
      <origin xyz="0 0 0" rpy="0 0 0"/>
      <inertia ixx="1e-6" ixy="0" ixz="0" iyy="1e-6" iyz="0" izz="1e-6" />
    </inertial>
  </link>

  <joint name="camera_joint" type="fixed">
    <axis xyz="0 1 0" />
    <origin xyz="${camera_link} 0 ${heightG - axel_offset*2}" rpy="0 0 0"/>
    <parent link="linkG"/>
    <child link="camera_link"/>
  </joint>

  <!-- Camera -->
  <link name="camera_link">
    <collision>
      <origin xyz="0 0 0" rpy="0 0 0"/>
      <geometry>
  	<box size="${camera_link} ${camera_link} ${camera_link}"/>
      </geometry>
    </collision>

    <visual>
      <origin xyz="0 0 0" rpy="0 0 0"/>
      <geometry>
  	<box size="${camera_link} ${camera_link} ${camera_link}"/>
      </geometry>
      <material name="red"/>
    </visual>

    <inertial>
      <mass value="1e-5" />
      <origin xyz="0 0 0" rpy="0 0 0"/>
      <inertia ixx="1e-6" ixy="0" ixz="0" iyy="1e-6" iyz="0" izz="1e-6" />
    </inertial>
  </link>

  <!-- generate an optical frame http://www.ros.org/reps/rep-0103.html#suffix-frames
       so that ros and opencv can operate on the camera frame correctly -->
  <joint name="camera_optical_joint" type="fixed">
    <!-- these values have to be these values otherwise the gazebo camera image
         won't be aligned properly with the frame it is supposedly originating from -->
    <origin xyz="0 0 0" rpy="${-pi/2} 0 ${-pi/2}"/>
    <parent link="camera_link"/>
    <child link="camera_link_optical"/>
  </joint>
  
  <link name="camera_link_optical">
  </link>
  
  <transmission name="tran0">
    <type>transmission_interface/SimpleTransmission</type>
    <joint name="joint0">
      <hardwareInterface>hardware_interface/EffortJointInterface</hardwareInterface>
    </joint>
    <actuator name="motor0">
      <hardwareInterface>hardware_interface/EffortJointInterface</hardwareInterface>
      <mechanicalReduction>1</mechanicalReduction>
    </actuator>
  </transmission>
  
  <transmission name="tran11">
    <type>transmission_interface/SimpleTransmission</type>
    <joint name="joint11">
      <hardwareInterface>hardware_interface/EffortJointInterface</hardwareInterface>
    </joint>
    <actuator name="motor11">
      <hardwareInterface>hardware_interface/EffortJointInterface</hardwareInterface>
      <mechanicalReduction>1</mechanicalReduction>
    </actuator>
  </transmission>

    <transmission name="tran12">
    <type>transmission_interface/SimpleTransmission</type>
    <joint name="joint12">
      <hardwareInterface>hardware_interface/EffortJointInterface</hardwareInterface>
    </joint>
    <actuator name="motor12">
      <hardwareInterface>hardware_interface/EffortJointInterface</hardwareInterface>
      <mechanicalReduction>1</mechanicalReduction>
    </actuator>
  </transmission>

  <transmission name="tran21">
    <type>transmission_interface/SimpleTransmission</type>
    <joint name="joint21">
      <hardwareInterface>hardware_interface/EffortJointInterface</hardwareInterface>
    </joint>
    <actuator name="motor21">
      <hardwareInterface>hardware_interface/EffortJointInterface</hardwareInterface>
      <mechanicalReduction>1</mechanicalReduction>
    </actuator>
  </transmission>

    <transmission name="tran22">
    <type>transmission_interface/SimpleTransmission</type>
    <joint name="joint22">
      <hardwareInterface>hardware_interface/EffortJointInterface</hardwareInterface>
    </joint>
    <actuator name="motor22">
      <hardwareInterface>hardware_interface/EffortJointInterface</hardwareInterface>
      <mechanicalReduction>1</mechanicalReduction>
    </actuator>
  </transmission>

  <transmission name="tran3">
    <type>transmission_interface/SimpleTransmission</type>
    <joint name="joint3">
      <hardwareInterface>hardware_interface/EffortJointInterface</hardwareInterface>
    </joint>
    <actuator name="motor3">
      <hardwareInterface>hardware_interface/EffortJointInterface</hardwareInterface>
      <mechanicalReduction>1</mechanicalReduction>
    </actuator>
  </transmission>

  <transmission name="finger1_tran">
    <type>transmission_interface/SimpleTransmission</type>
    <joint name="finger_joint1">
      <hardwareInterface>hardware_interface/EffortJointInterface</hardwareInterface>
    </joint>
    <actuator name="finger_motor1">
      <hardwareInterface>hardware_interface/EffortJointInterface</hardwareInterface>
      <mechanicalReduction>1</mechanicalReduction>
    </actuator>
  </transmission>

  <transmission name="finger2_tran">
    <type>transmission_interface/SimpleTransmission</type>
    <joint name="finger_joint2">
      <hardwareInterface>hardware_interface/EffortJointInterface</hardwareInterface>
    </joint>
    <actuator name="finger_motor2">
      <hardwareInterface>hardware_interface/EffortJointInterface</hardwareInterface>
      <mechanicalReduction>1</mechanicalReduction>
    </actuator>
  </transmission>
  
</robot>
