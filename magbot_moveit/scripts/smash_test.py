#!/usr/bin/env python

import roslib;
#roslib.load_manifest('smach_tutorials')
import rospy
import smach
import smach_ros

import sys
import copy
import rospy
import moveit_commander
import moveit_msgs.msg
import geometry_msgs.msg

class Moving(smach.State):
    def __init__(self, group, robot, display_trajectory_publisher):
        self.group = group
        self.robot = robot
        self.display_trajectory_publisher = display_trajectory_publisher
        smach.State.__init__(self, outcomes=['stopped', 'in_move'])
        # Your state initialization goes here
        self.MAX_COUNT = 3
        self.counter = 0;
         
    def execute(self, userdata):
        # Your state execution goes here
        if self.counter < self.MAX_COUNT:
            self.counter += 1

            pose_target = self.group.get_random_pose().pose
            self.group.set_pose_target(pose_target)
            
            my_plan = self.group.plan();
            if my_plan:
                self.group.execute(my_plan);
                display_trajectory = moveit_msgs.msg.DisplayTrajectory()
                display_trajectory.trajectory_start = self.robot.get_current_state()
                display_trajectory.trajectory.append(my_plan)
                self.display_trajectory_publisher.publish(display_trajectory);
                rospy.sleep(5)


            return 'in_move'
        else:
            return 'stopped'

class Done(smach.State):
    def __init__(self):
        # Your state initialization goes here
        smach.State.__init__(self, outcomes=['done'])        

        
    def execute(self, userdata):
        rospy.loginfo('Executing state Done')
        return 'done'

def main():
    
    rospy.init_node('smach_moveit_test', anonymous=True)
    moveit_commander.roscpp_initialize(sys.argv)
    robot = moveit_commander.RobotCommander()
    scene = moveit_commander.PlanningSceneInterface()
    group = moveit_commander.MoveGroupCommander("arm2")
    display_trajectory_publisher = rospy.Publisher(
                                      '/move_group/display_planned_path',
                                      moveit_msgs.msg.DisplayTrajectory,
                                      queue_size=20)
    sm = smach.StateMachine(outcomes=['done'])
    
    with sm:
        smach.StateMachine.add('Moving', Moving(group, robot, display_trajectory_publisher),
                               transitions={'in_move':'Moving',
                                            'stopped':'Done'})
        smach.StateMachine.add('Done', Done(),
                               transitions={})
        # Execute SMACH plan
        outcome = sm.execute()

if __name__ == '__main__':
    main ();
    
